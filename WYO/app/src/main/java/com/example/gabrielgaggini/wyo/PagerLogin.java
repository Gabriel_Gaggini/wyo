package com.example.gabrielgaggini.wyo;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class PagerLogin extends FragmentStatePagerAdapter
{
    /** Attributs de la Class **/
    private int tabCount;



    /** Constructeur **/
    public PagerLogin (FragmentManager fm, int tabCount)
    {
        super(fm);
        this.tabCount   =   tabCount;
    }




    /** Methode de la Class **/
    @Override
    public Fragment getItem(int position)
    {
        switch (position)
        {
            case 0: return new ConnexionFragment();
            case 1: return new InscriptionFragment();
            default: return null;
        }
    }



    @Override
    public int getCount() { return this.tabCount; }

// Fin de la Class
}
