package com.example.gabrielgaggini.wyo;

import android.net.Uri;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class LoginActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener, ConnexionFragment.OnFragmentInteractionListener, InscriptionFragment.OnFragmentInteractionListener

{

    /** Attributs de la Class **/
    private TabLayout tabLayoutLogin;
    private ViewPager viewPagerLogin;
    private ImageButton buttonInformation;



    /** Création de l'Activity**/
    @Override protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Masque l'App-Bar de l'activity si elle y est
        if(this.getSupportActionBar() != null) { this.getSupportActionBar().hide(); }

        // Prepare les ID de l'activity avec les ecoute sur les boutons
        this.prepareActivity();

    }



    /** Methode qui va récuperer les éléments de l'activity **/
    private void prepareActivity()
    {
        // Récupere les ID de l'activity
        this.tabLayoutLogin     = findViewById(R.id.tab_layout_login);
        this.viewPagerLogin     = findViewById(R.id.view_pager_login);
        this.buttonInformation  = findViewById(R.id.inscription_information);

        // Ajout des click listener
        this.buttonInformation
                .setOnClickListener(this.onClickListener);



        // Insertion des sections dans la TabLayout
        this.tabLayoutLogin
                .addTab(this.tabLayoutLogin.newTab().setText("Connexion"));
        this.tabLayoutLogin
                .addTab(this.tabLayoutLogin.newTab().setText("Inscription"));

        // Ajout des parametre d'affichage de la TabLayout
        this.tabLayoutLogin
                .setTabGravity(TabLayout.GRAVITY_FILL);
        this.tabLayoutLogin
                .addOnTabSelectedListener(LoginActivity.this);


        // Création d'un adapter pour le View Pager
        PagerLogin adapter   =   new PagerLogin(this.getSupportFragmentManager(), this.tabLayoutLogin.getTabCount());


        // Ajout de l'adapter au View Pager
        this.viewPagerLogin.setAdapter(adapter);

        // Ajout du swipe sur le View Pager
        this.viewPagerLogin.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(this.tabLayoutLogin));
    }





    /** Création d'un objet de type listener qui va ecouter les click sur mes boutons **/
    View.OnClickListener onClickListener    =   new View.OnClickListener()
    {
        @Override public void onClick(View view)
        {
            switch (view.getId())
            {
                // Création d'un dialogue
                case R.id.inscription_information :
                    Utils.createSimpleDialog(R.string.info_donnes_gens, LoginActivity.this);
                    break;
            }

        }
    };






    /** Methode redéfini de la Tab Layout **/
    @Override
    public void onTabSelected(TabLayout.Tab tab) { this.viewPagerLogin.setCurrentItem(tab.getPosition()); }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) { }

    @Override
    public void onTabReselected(TabLayout.Tab tab) { }


    /** Interface des Fragment **/
    @Override
    public void onFragmentInteraction(Uri uri) { }


// Fin de la Class
}
