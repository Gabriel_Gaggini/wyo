package com.example.gabrielgaggini.wyo;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import de.hdodenhof.circleimageview.CircleImageView;



public class CustomViewComment extends LinearLayout
{
    /** Attributs de la Class **/
    private CircleImageView imageComment;
    private TextView        pseudoComment;
    private TextView        contentComment;


    /** Constrcuteur **/
    public CustomViewComment(Context context)
    {
        super(context);
        // Prepare et recupere les ID
        this.prepareView(context);
    }

    /** Constrcuteur **/
    public CustomViewComment(Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);
        // Prepare et recupere les ID
        this.prepareView(context);
    }

    /** Constrcuteur **/
    public CustomViewComment(Context context, @Nullable AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        // Prepare et recupere les ID
        this.prepareView(context);
    }


    /** Methode qui va récuperer les elements de ma Vue **/
    private void prepareView(Context asContext)
    {
        // Monte la View
        View customComment  = LayoutInflater.from(asContext).inflate(R.layout.activity_custom_view_comment, this, true);

        // Récupere les ID
        this.imageComment   =   (CircleImageView)   customComment.findViewById(R.id.imageComment);
        this.pseudoComment  =   (TextView)          customComment.findViewById(R.id.pseudoComment);
        this.contentComment =   (TextView)          customComment.findViewById(R.id.contenuComment);
    }


    /** Methode qui va inscrire les données dans les vue **/
    public void setDataOnView(String imagePseudo, String pseudoComment, String contentComment)
    {
        // Inscription des données
        this.pseudoComment.setText(pseudoComment);
        this.contentComment.setText(contentComment);

        Picasso.get()
                .load(imagePseudo)
                .into(this.imageComment);
    }




// Fin de la Class
}
