package com.example.gabrielgaggini.wyo;

public interface DataSendServer
{
    void dataSendSuccess(String serverReponse);
    void dataSendFailed();
}
