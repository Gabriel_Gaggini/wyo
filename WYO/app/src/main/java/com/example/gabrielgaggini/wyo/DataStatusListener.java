/** Interface qui va servir a remonter des information via les differnetes class **/
/** Made by Gabriel Gaggini **/

package com.example.gabrielgaggini.wyo;

import java.util.ArrayList;

public interface DataStatusListener
{
    void dataSucessRetrieved(ArrayList<Status> articles);
    void dataNotSucessRetrieved();
}
