/** Interface qui va permettre de remonter l'information des Click
 *  Made By Gabriel Gaggini
 *  **/

package com.example.gabrielgaggini.wyo;

import java.util.List;

public interface RecylerClickListener
{
    void onClickItemRecycler(int position, List<Status> statutsList);
    
}
