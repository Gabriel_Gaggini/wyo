/******************************************************/
/** Page qui va contenir les methodes
 *  qui eront utils pour tout le monde
 *
 *  Made By Gabriel Gaggini
 *  Project Name : WYO**/
/******************************************************/

package com.example.gabrielgaggini.wyo;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import org.apache.commons.io.IOUtils;
import java.io.InputStream;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.nio.charset.Charset;

public class Utils
{

    /** Methode qui va enlever les animations de la Bottom Navigation View **/
    @SuppressLint("RestrictedApi")
    public static void cancelAnimationBottomBar(BottomNavigationView view)
    {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);

            try
            {
                Field   shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");

                        shiftingMode.setAccessible(true);

                        shiftingMode.setBoolean(menuView, false);

                        shiftingMode.setAccessible(false);


                //Parcours des items de la BottomNavigationBar boucle qui va mettre a jour les item avec les nouvelle configuration
                for (int i = 0 ; i < menuView.getChildCount() ; i++)
                {
                    BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);

                    //noinspection RestrictedApi
                    item.setShiftingMode(false);

                    // Met a jour les item de la BottomBarNavigation
                    item.setChecked(item.getItemData().isChecked());
                }
            }

            //Exception si sa n'aboutit pas
            catch (NoSuchFieldException e) { Log.e("BNVHelper", "Unable to get shift mode field", e); }
            catch (IllegalAccessException e) { Log.e("BNVHelper", "Unable to change value of shift mode", e); }
    }





    /** Vérifie si la chaine de caractère est correct **/
    public static boolean isValidString(String stringVerif) { return stringVerif != null && !stringVerif.isEmpty(); }






    /** Va convertir le JSON recu en String et va le renvoyer **/
    public static String convertJsonOnString(InputStream jsonToConvert)
    {
        try
        {
            StringWriter stringWriter = new StringWriter();
            IOUtils.copy(jsonToConvert, stringWriter, String.valueOf(Charset.forName("UTF-8")));

            // Renvoi la chaine de caractère
            return stringWriter.toString();
        }
        catch (Exception e) { return null; }
    }







    /** Methode qui crée les dialogues simple **/
    public static void createSimpleDialog(int displayMessage, Context contextApp)
    {
        AlertDialog.Builder alertBuilder    =   new AlertDialog.Builder(contextApp);

        // Ajout d'un message et d'un bouton sur le dialogue
        alertBuilder.setMessage(displayMessage)
                .setPositiveButton("Ok", null);

        // Affichage du dialog
        alertBuilder.show();
    }




    /** Methode qui crée les CustomDialog **/
    public static void createCustomDialogInformation(String contenu,String title, Context context)
    {
        if(context != null)
        {
            AlertDialog.Builder alertBuilder    =   new AlertDialog.Builder(context);

            CustomContentDialog customContentDialog =   new CustomContentDialog(context, contenu);

            // Ajout d'un message et d'un bouton sur le dialogue
            alertBuilder
                    .setView(customContentDialog)
                    .setPositiveButton("Ok", null);

            CustomTitleDialog dialogCustomTitle =   new CustomTitleDialog(context, title);

            alertBuilder
                    .setCustomTitle(dialogCustomTitle);

            // Affichage du dialog
            alertBuilder.show();
        }
        else { Log.e("createDialog", "Le context est null"); }
    }


    /** Methode qui va crée un dialog pour changer de mot de passe **/
    public static void createCustoDialogResetPassword(Context context)
    {
        if(context != null)
        {
            AlertDialog.Builder alertBuilder    =   new AlertDialog.Builder(context);

            // Ajout d'un message et d'un boutons sur le dialogue
            alertBuilder
                    .setView(new CustomDialogResetPassword(context))
                    .setCustomTitle(new CustomTitleDialog(context, "Changer de mot de passe"))
                    .setPositiveButton("Accepter", null)
                    .setNegativeButton("Annuler", null);

            alertBuilder.show();
        }
    }



// Fin de la Class
}