/** Class utilisé pour créer des vue custom pour l'affichage des Statuts
 *  Made By Gabriel Gaggini
 *  **/

package com.example.gabrielgaggini.wyo;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.squareup.picasso.Picasso;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.UnsupportedEncodingException;
import java.util.List;
import de.hdodenhof.circleimageview.CircleImageView;


public class StatusCustomCellView extends RelativeLayout implements DeleteStatutsClickListener, DataSendServer
{

    /** Attributs de la Class **/
    private     static  final   int             LAYOUT_ID       =   R.layout.custom_status_view;
    private                     boolean         stateHeart      =   false;
    private                     CircleImageView imageProfil;
    private                     TextView        auteurStatus;
    private                     TextView        categorieStatus;
    private                     TextView        contenuStatus;
    private                     ImageView       imageStatus;
    private                     TextView        nbComment;
    private                     TextView        nbLike;
    private                     int             intNbLike;
    private                     ImageView       heart;
    private                     TextView        date;
    private                     TextView        heure;
    private                     ImageButton     cross_delete;




    /** Constructeur **/
    public StatusCustomCellView(Context context)
    {
        super(context);

        // Récupere les id des élements de la vue
        this.getIdOfActivityElement(context);
    }


    /** Constructeur **/
    public StatusCustomCellView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        // Récupere les id des élements de la vue
        this.getIdOfActivityElement(context);
    }



    /** Methode qui va récuperer les élément de l'activity **/
    private void getIdOfActivityElement(Context asContext)
    {
        // Monte la vue a l'ecran
        View customViewCell  = LayoutInflater.from(asContext).inflate(LAYOUT_ID, this, true);

        // Récupere les ID
        this.heart              =   (ImageButton)     customViewCell.findViewById(R.id.heart);
        this.auteurStatus       =   (TextView)        customViewCell.findViewById(R.id.auteur);
        this.imageProfil        =   (CircleImageView) customViewCell.findViewById(R.id.image);
        this.contenuStatus      =   (TextView)        customViewCell.findViewById(R.id.contenu);
        this.categorieStatus    =   (TextView)        customViewCell.findViewById(R.id.status_categorie);
        this.imageStatus        =   (ImageView)       customViewCell.findViewById(R.id.image2);
        this.date               =   (TextView)        customViewCell.findViewById(R.id.status_date);
        this.heure              =   (TextView)        customViewCell.findViewById(R.id.status_heure);
        this.nbLike             =   (TextView)        customViewCell.findViewById(R.id.nb_heart);
        this.nbComment          =   (TextView)        customViewCell.findViewById(R.id.nb_comment);
        this.cross_delete       =   (ImageButton)     customViewCell.findViewById(R.id.cross_delete);

        // Ajout d'un click sur le boutons du coeur
        this.heart.setOnClickListener(this.onClickListener);
        this.auteurStatus.setOnClickListener(this.onClickListener);

    }




    /** Objet de type CLick Listener qui va ecouter les click sur les éléments **/
    View.OnClickListener onClickListener    =   new OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            switch (v.getId())
            {
                case R.id.heart:
                                // Change la couleur du contour du coeur des status
                                if(!stateHeart)
                                {
                                    heart.setColorFilter(Color.RED);
                                    intNbLike++;
                                    updateNbLikeView();
                                    stateHeart = true;
                                }
                                else
                                {
                                    heart.setColorFilter(Color.BLACK);
                                    intNbLike--;
                                    updateNbLikeView();
                                    stateHeart = false;
                                }
                                break;

                case R.id.auteur:
                                Toast.makeText(getContext(), auteurStatus.getText().toString(), Toast.LENGTH_LONG).show();



            }
        }
    };


    /** Methode qui va inscirre les données dans la vue **/
    public void setDataOnCustomView(String auteurStatus, String urlContenuImage,  String urlProfilImage, String contenu, String categorieStatus, String date, String heure, int nbLike, int nbComment)
    {
        // Ajout du nombre de Like
        this.intNbLike  =   nbLike;

        // Ajout des données
        this.auteurStatus.setText(auteurStatus);
        this.contenuStatus.setText(contenu);
        this.categorieStatus.setText(categorieStatus);
        this.date.setText(date);
        this.heure.setText(heure);
        this.nbLike.setText(String.valueOf(this.intNbLike));
        this.nbComment.setText(String.valueOf(nbComment));

        // Chargment et ajut des photos
        Picasso.get()
                .load(urlContenuImage)
                .into(this.imageStatus);

        Picasso.get()
                .load(urlProfilImage)
                .into(this.imageProfil);
    }


    /** Met a jour la view des Likes **/
    private void updateNbLikeView() { this.nbLike.setText(String.valueOf(this.intNbLike)); }


    /** Getters de la Class **/
    public ImageView getImageStatus() { return this.imageStatus; }

    public ImageButton getButtonDelete() { return this.cross_delete; }



    /** Methode qui va supprimer un statuts **/
    @Override public void onClickToDeleteStatuts(int position, List<Status> statutsList)
    {
        String id   =   "" + Math.random();

        // Ajout de la liste avec un id et la liste des statuts
        ContentHolder.getInstance().setStatutsList(id, statutsList);

        Status statusDelete     =   ContentHolder.getInstance().getStatuts(id).get(position);

            try
            {
                ContentManager.deleteStatuts(this, this.prepareData(statusDelete.getsId()));
            }
            catch (UnsupportedEncodingException e)
            {
                e.printStackTrace();
            }

        Toast.makeText(getContext(), ""+ statusDelete.getsId(), Toast.LENGTH_LONG).show();

    }


    /** Methode qui va préparer le json a envoyer **/
    private JSONObject prepareData(int sId)
    {
        JSONObject jsonObject   =   new JSONObject();

        try
        {
            jsonObject.put("sId", sId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override
    public void dataSendSuccess(String serverReponse) {

    }

    @Override
    public void dataSendFailed() {

    }


// Fin de la Class
}


