package com.example.gabrielgaggini.wyo;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>
{

    /** Attributs de la Class **/
    // List qui va contenir mes Articles
    private                     ArrayList<Status>           statusList      =   new ArrayList<>();
    // Listener
    private                     RecylerClickListener        clickListenerOnStatus;
    private                     DeleteStatutsClickListener  deleteStatutsClickListener;
    private                     int                         uId;



    /** Garde ma vue (Cellule) **/
    public static class ViewHolder extends RecyclerView.ViewHolder { public ViewHolder(View view) { super(view);  }}



    /** Constructeur du RecyclerViewAdapter **/
    public RecyclerViewAdapter(RecylerClickListener clickListener,DeleteStatutsClickListener deleteStatutsClickListener, int uId)
    {
        this.uId                        =   uId;
        this.clickListenerOnStatus      =   clickListener;
        this.deleteStatutsClickListener =   deleteStatutsClickListener;
    }





    /** Methode de la RecylerView redéfini **/

    /** Methode qui crée les cellules de la RecyclerView **/
    @NonNull
    @Override public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        // Création d'une cellule custom
        View customViewCell  =   new StatusCustomCellView(parent.getContext());

        // Création des parametres
        RecyclerView.LayoutParams   parametersView  =   new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        // Ajout des parametres a la vue
        customViewCell.setLayoutParams(parametersView);

        return new ViewHolder(customViewCell);
    }





    /** Methode qui va envoyer les données **/
    @Override public void onBindViewHolder(@NonNull RecyclerViewAdapter.ViewHolder holder, @SuppressLint("RecyclerView") final int position)
    {
        // Je crée une cellule custom pour chaque élément de ma Recycler View
        final   StatusCustomCellView   customStatuts   =   (StatusCustomCellView) holder.itemView;

            // On regarde si la personne connecter et l'auteur du statuts
            /** if(this.statusList.get(position).getuId() == this.uId)
            {
                // On change la visibilité de la croix sur le statuts
                customStatuts.getButtonDelete().setVisibility(View.VISIBLE);
                this.statusList.remove(position);
                // Ajout d'un click listener sur la croix pour supprimer
                customStatuts.getButtonDelete().setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        deleteStatutsClickListener.onClickToDeleteStatuts(position, statusList);
                    }
                });
            } **/

        // Envoi des données pour l'inscription de la data en utilisant le parametre position
        customStatuts.setDataOnCustomView(this.statusList.get(position).getAuteur(), this.statusList.get(position).getUrlContenuImage() ,this.statusList.get(position).getUrlProfilImage(), this.statusList.get(position).getsContenu(), this.statusList.get(position).getCategorie(), this.statusList.get(position).getsDate(), this.statusList.get(position).getsHeure(), this.statusList.get(position).getsLike(), this.statusList.get(position).getNbComment());

        // Ajout du click sur le statuts
        customStatuts.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                clickListenerOnStatus.onClickItemRecycler(position,statusList);
            }
        });
    }






    /** Renvoi le nombre le nombre d'item que va contenir la Recycler View **/
    @Override public int getItemCount() { return this.statusList.size() ; }




    /** Methode qui va refresh la liste d'Article **/
    public void adapterRefreshListArticle(ArrayList<Status> newListArticle)
    {
        // Ajout de la nouvelle renvoyer par le GetJsonOnline
        this.statusList    =   newListArticle;

        // Permet de dire a l'adapter qu'il doit se refresh
        notifyDataSetChanged();
    }


// Fin de la Class
}
