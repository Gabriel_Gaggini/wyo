package com.example.gabrielgaggini.wyo;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class ThreadPoolGetComment implements Runnable, Comparable
{
    /** Attributs de la Class **/
    private         String              url;
    private         String              jsonToSend;
    private         DataCommentStatuts  dataCommentStatuts;
    private final   Handler             mMainHandler    = new Handler(Looper.getMainLooper());




    /** Constructeur **/
    public ThreadPoolGetComment(String url, DataCommentStatuts dataCommentStatuts, JSONObject jsonObject) throws UnsupportedEncodingException {
        this.url                =   url;
        this.dataCommentStatuts =   dataCommentStatuts;
        // J'encode le JSON en utf-8
        this.jsonToSend         =   URLEncoder.encode(jsonObject.toString(), "UTF-8");
    }




    @Override public void run()
    {
        HttpURLConnection httpURLConnection = null;

        try
        {
            if(!Utils.isValidString(this.url)) { return; }

            // Url du serveur
            URL url             =   new URL(this.url);

            // Ouvre la connexion avec le serveur
            httpURLConnection       =   (HttpURLConnection) url.openConnection();
            // J'anonce que cela va etre une requete POST
            httpURLConnection.setRequestMethod("POST");
            // Ajout d'un temps d'attente maximum
            httpURLConnection.setConnectTimeout(10000);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.connect();


            // Objet qui permet d'envoyer les données
            DataOutputStream wr     =   new DataOutputStream(httpURLConnection.getOutputStream());


            // Ajout des données
            wr.writeBytes("statutData="+ jsonToSend);

            wr.flush();
            wr.close();

            // Vérifie le status de la requetes
            if(httpURLConnection.getResponseCode() != 200)
            {
                this.commentNotSuccess();
                return;
            }


            // Récupere la data afficher a l'ecran
            InputStream inputStream =   httpURLConnection.getInputStream();

            // J'extrait le tout en String
            String      reponse     =   Utils.convertJsonOnString(inputStream);

            // verifie que la reponse n'est pas null
            if(reponse != null)
            {
                this.commentSuccess(reponse);
            }


        } catch (IOException e) { e.printStackTrace(); }
    }








    /** Methode appeler lorsque les données arrive **/
    private void commentSuccess(final String stringComment)
    {
        final DataCommentStatuts        dataCommentStatuts      =   this.dataCommentStatuts;

        final ArrayList<Commentaire>    arrayListComment        =   ContentManager.convertJsonOnCommentaire(stringComment);

            if(dataCommentStatuts != null)
            {
                this.mMainHandler.post(new Runnable()
                {
                    @Override public void run() {
                        dataCommentStatuts.commentReceive(arrayListComment);
                    }
                });
            }
    }


    /** Methode appeler si un problème est survenu **/
    private void commentNotSuccess() {}





    @Override
    public int compareTo(@NonNull Object o) { return 0; }



// Fin de la Clas
}
