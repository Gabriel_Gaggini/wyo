/** Class qui va afficher le detail d'un article
 *  Made By Gabriel Gaggini
 */

package com.example.gabrielgaggini.wyo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import org.json.JSONObject;
import java.util.ArrayList;


public class DetailStatut extends AppCompatActivity implements DataCommentStatuts
{
    /** Clé pour récuperer le statuts dans l'Intent **/
    private static      final   String                  STATUS_CLICK            =   "statuts_click";
    private                     RecyclerView            recyclerComment;
    private                     Status                  statusClick;
    private                     StatusCustomCellView    statusCustomCellView;
    private                     RecyclerAdapterComment  recyclerAdapterComment;





    /** Methode qui crée l'activity **/
    @Override protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_statut);
        this.setTitle("What's Your Opinion");

        // Prepare la Vue
        this.prepareView();
    }




    /** Methode qui va préparer le Vue**/
    private void prepareView()
    {
        // Recupere les ID
        this.recyclerComment        =   (RecyclerView) findViewById(R.id.recyler_commentaire);
        this.statusCustomCellView   =   (StatusCustomCellView) findViewById(R.id.comment_custom_statuts);


        // Récupere la réfrence du Statuts passer en Extra dans l'Intent
        this.statusClick            =   (Status) getIntent().getSerializableExtra(STATUS_CLICK);

        // Ajout des données
        this.statusCustomCellView.setDataOnCustomView(statusClick.getAuteur(), statusClick.getUrlContenuImage(), statusClick.getUrlProfilImage(), statusClick.getsContenu(), statusClick.getCategorie(), statusClick.getsDate(), statusClick.getsHeure(), statusClick.getsLike(), statusClick.getNbComment());


        // Création d'un adapter
        this.recyclerAdapterComment  =   new RecyclerAdapterComment();
        // Ajout de l'adapter
        this.recyclerComment.setAdapter(this.recyclerAdapterComment);
        // Ajout des parametre de l'affichage de la recycler view
        this.recyclerComment.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));

        // Récupere de l'ID du statuts cliquer et appel de la renquete pour recuperer les commentaires
        this.getCommentOfStatutClick(statusClick);

        // Ajout du click pour faire disparaitre l'image
        this.statusCustomCellView.getImageStatus().setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                statusCustomCellView.getImageStatus().setVisibility(View.GONE);
            }
        });
    }


    /** Methode qui appeler la requete pour récuperer les commentaire **/
    private void getCommentOfStatutClick(Status status)
    {
        try
        {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("sId", status.getsId());

            ContentManager.getCommentForStatuts(this, jsonObject);
        }
        catch (Exception e) { Log.e(DetailStatut.class.getSimpleName(), "Crash"); }
    }



    /** Methode de l'interface qui remonte l'information des commentaires **/
    @Override public void commentReceive(ArrayList<Commentaire> arrayListComment)
    {
        this.recyclerAdapterComment.refreshListComment(arrayListComment);
    }

    @Override public void commentNotReceive() { }


// Fin de la Class
}
