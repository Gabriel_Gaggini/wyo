package com.example.gabrielgaggini.wyo;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


public class ProfilInformationUser extends Fragment
{

    /** Attributs de la Class **/
    // TAG QUI CORRESPOND A LA CLE POUR AVOIR ACCES A MES ARGUMENTS
    private static final    String      USER_CONNECT = "user_connect";
    private                 String      mParam1;
    private                 TextView    user_nom;
    private                 TextView    user_prenom;
    private                 TextView    user_mail;
    private                 TextView    user_date_naissance;
    private                 TextView    user_sexe;
    private                 OnFragmentInteractionListener mListener;




    /** Constructeur **/
    public ProfilInformationUser() { }


    /** Methode static pour crée un Fragment **/
    public static ProfilInformationUser newInstance(User userConnect)
    {
        ProfilInformationUser   fragment    =   new ProfilInformationUser();

        Bundle                  args        =   new Bundle();
                                args.putSerializable(USER_CONNECT, userConnect);

        fragment.setArguments(args);

        return fragment;
    }



    /** Methode de création du Fragment **/
    @Override public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

            if (getArguments() != null) { mParam1 = getArguments().getString(USER_CONNECT); }
    }





    /** Methode qui va monter la vue du Fragment **/
    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View    information_user =   inflater.inflate(R.layout.profil_information_user_fragment, container, false);

        this.getElementOfFragment(information_user);
        this.writeDataOnElement();
        return information_user;
    }





    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri)
    {
        if (mListener != null) { mListener.onFragmentInteraction(uri); }
    }



    /** Methode qui va récuperer les éléments du Fragment **/
    private void getElementOfFragment(View view)
    {
        // Récupere les éléments
        this.user_nom               =   (TextView) view.findViewById(R.id.nom_propos);
        this.user_prenom            =   (TextView) view.findViewById(R.id.prenom_propos);
        this.user_mail              =   (TextView) view.findViewById(R.id.mail_propos);
        this.user_date_naissance    =   (TextView) view.findViewById(R.id.date_naissance_propos);
        this.user_sexe              =   (TextView) view.findViewById(R.id.sexe_propos);
    }


    /** Methode qui va inscrire les données dans les TextView **/
    private void writeDataOnElement()
    {
        // Verifie que les arguments du fragment ne sont pas vide
        if(getArguments() != null)
        {
            User    userInfo    =   (User) this.getArguments().getSerializable(USER_CONNECT);

            // Verifie que le User n'est pas vide
            if(userInfo != null)
            {
                this.user_nom.setText(userInfo.getuNom());
                this.user_prenom.setText(userInfo.getuPrenom());
                this.user_mail.setText(userInfo.getuMail());
                this.user_date_naissance.setText(userInfo.getuDateNaissance());
                this.user_sexe.setText(userInfo.getuSexe());
            }
            else { Toast.makeText(getContext(), "Aucune données se trouve dans le User", Toast.LENGTH_LONG).show(); }

        }
        else { Toast.makeText(getContext(), "Aucune données se trouve dans les arguments", Toast.LENGTH_LONG).show(); }

    }



    /** Methode appeler au moment de l'attachement d'un Fragment **/
    @Override public void onAttach(Context context)
    {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) { mListener = (OnFragmentInteractionListener) context; }
        else { throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener"); }
    }



    @Override
    public void onDetach()
    {
        super.onDetach();
        mListener = null;
    }


    /** Interface du Fragment a implémenter **/
    public interface OnFragmentInteractionListener { void onFragmentInteraction(Uri uri);}


// Fin de la Class
}
