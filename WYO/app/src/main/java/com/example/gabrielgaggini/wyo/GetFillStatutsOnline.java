package com.example.gabrielgaggini.wyo;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.util.Log;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class GetFillStatutsOnline implements Runnable, Comparable
{

    /** Attributs de la Class **/
    private         WeakReference<DataStatusListener> listenerWeakReference;
    private         String                              urlJson;
    private final   Handler mMainHandler = new Handler(Looper.getMainLooper());




    /** Constructeur de la Class **/
    public GetFillStatutsOnline(String urlJson, DataStatusListener listener)
    {
        super();
        this.listenerWeakReference  =   new WeakReference<>(listener);
        this.urlJson                =   urlJson;
    }


    /** Methode qui va aller chercher le Json en ligne **/
    @Override public void run()
    {
        // Verifie que l'URL est bonne
        if (!Utils.isValidString(this.urlJson))
        {
            Log.e("GetJsonOnline", "L'url n'est pas valide");
            return;
        }

        // On essaye de recuperer le Json en ligne
        try
        {
            // Création d'un objet de type URL
            URL urlForJson  =   new URL(this.urlJson);

            // Création d'un objet HTTP qui va ouvrir la connection
            HttpURLConnection   connection    =   (HttpURLConnection) urlForJson.openConnection();
                                connection
                                        .setRequestMethod("GET");
                                connection
                                        .setReadTimeout(10000);
                                connection
                                        .setConnectTimeout(10000);
                                connection
                                        .connect();


            // Vérifie si le code d'erreur est differents de 200 (OK)
            if(connection.getResponseCode() != 200)
            {
                this.statusNotRetrieved();
                return;
            }

            // Récupere le JSON de l'API
            InputStream inputStream     =   connection.getInputStream();

            // Convertiti le JSON en String
            String      jsonOnString    =   Utils.convertJsonOnString(inputStream);

            // Array List qui va contenir mon JSON
            ArrayList<Status> jsonList  =   ContentManager.convertJsonOnStatuts(jsonOnString);


            // Verifie que rien n'est vide
            if(jsonList != null && !jsonList.isEmpty()) { this.statusRetrieved(jsonList); }
        }
        catch (Exception e) { Log.e("GetJsonOnline", "Impossible de récuperer le JSON online"); }
    }


    /** Methode qui va dire si mes status ont été recu **/
    private void statusRetrieved(final ArrayList<Status> listJson)
    {
        final DataStatusListener listener = this.listenerWeakReference.get();

            if (listener != null)
            {
                mMainHandler.post(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        listener.dataSucessRetrieved(listJson);
                    }
                });
            }
    }


    /** Methode qui va dire si mes status ont été recu **/
    private void statusNotRetrieved()
    {
        final DataStatusListener listener = this.listenerWeakReference.get();

            if (listener != null)
            {
                mMainHandler.post(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        listener.dataNotSucessRetrieved();
                    }
                });
            }
    }




    @Override public int compareTo(@NonNull Object o) { return 0; }




// Fin de la Class


}
