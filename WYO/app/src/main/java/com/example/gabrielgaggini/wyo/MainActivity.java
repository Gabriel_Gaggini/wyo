/******************************************************/
/** Page de Naviguation de l'Application
 *  Cette Activity va contenir les différents Fragment
 *  néccesaire au fonctionnement de l'applictin
 *
 *  Made By Gabriel Gaggini
 *  Project Name : WYO**/
/******************************************************/

package com.example.gabrielgaggini.wyo;


import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import android.support.v4.app.Fragment;

public class MainActivity extends AppCompatActivity implements HomeFragment.OnFragmentInteractionListener, ProfilFragment.OnFragmentInteractionListener, ProfilInformationUser.OnFragmentInteractionListener, AddStatusFragment.OnFragmentInteractionListener
{
    // TAG QUI CORRESPOND AU EXTRA DE L'INTENT
    private static final String USER_CONNECT    = "userIsConnect";


    /** Action sur la Bottom Bar **/
    private BottomNavigationView.OnNavigationItemSelectedListener  onNavigationItemSelectedListener     =   new BottomNavigationView.OnNavigationItemSelectedListener()
    {
        /** Methode qui va s'éxecuter au moment d'un clic sur les icones de la Bottom Br **/
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item)
        {
            // Methode qui selectionne le Fragment requis
            return selectFragmentOnCLick(item);
        }
    };




    /** Création de l'activity **/
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Ajout du titre a l'ActionBar
        this.setTitle("What's Your Opinion");

        BottomNavigationView    bottomNavigationView    = findViewById(R.id.bottom_navigation);
        Utils.cancelAnimationBottomBar(bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener);

        // Création d'un nouveau Fragment Home
        Fragment fragment    =   HomeFragment.newInstance((User) getIntent().getSerializableExtra(USER_CONNECT));
        // Ajout du Fragment a l'activity
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_replace, fragment)
                .commit();



    }

    /** @Override  public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_option_bar, menu);
        return true;
    } **/




    /** Selectionne le Fragment requis et renvoi true si le fragment est crée **/
    private Boolean selectFragmentOnCLick(MenuItem icon)
    {
        Fragment fragmentAtDisplay  =   null;

        switch (icon.getItemId())
        {
            case R.id.navigation_home:
                                        if(icon.isChecked()) { return false; }

                                        // Ajout du titre a l'ActionBar
                                        this.setTitle("What's Your Opinion");
                                        // Création d'un nouveau Fragment Home
                                        fragmentAtDisplay    =   HomeFragment.newInstance((User) getIntent().getSerializableExtra(USER_CONNECT));
                                        // Ajout du Fragment a l'activity
                                        getSupportFragmentManager()
                                                .beginTransaction()
                                                .replace(R.id.fragment_replace, fragmentAtDisplay)
                                                .commit();

                                        return true;

            case R.id.navigation_categorie:
                                        if(icon.isChecked()) { return false; }

                                        // Ajout du titre a l'ActionBar
                                        this.setTitle("What's Your Opinion");
                                        Toast.makeText(getApplicationContext(), "Categorie CLicked", Toast.LENGTH_SHORT).show();
                                        return true;

            case R.id.navigation_add_post:
                                        if(icon.isChecked()) { return false; }

                                        // Ajout du titre a l'ActionBar
                                        this.setTitle("What's Your Opinion");
                                        // Création d'un nouveau Fragment Home
                                        fragmentAtDisplay    =   AddStatusFragment.newInstance((User) getIntent().getSerializableExtra(USER_CONNECT));
                                        // Ajout du Fragment a l'activity
                                        getSupportFragmentManager()
                                                .beginTransaction()
                                                .replace(R.id.fragment_replace, fragmentAtDisplay)
                                                .commit();

                                        return true;

            case R.id.navigation_setting:
                                        if(icon.isChecked()) { return false; }

                                        // Ajout du titre a l'ActionBar
                                        this.setTitle("What's Your Opinion");
                                        Toast.makeText(getApplicationContext(), "Setting CLicked", Toast.LENGTH_SHORT).show();
                                        return true;

            case R.id.navigation_profil:
                                        if(icon.isChecked()) { return false; }

                                        User    user    =   (User) getIntent().getSerializableExtra(USER_CONNECT);
                                        this.setTitle(user.getuPseudo());

                                        // Appel de la methode static pour le crée le fragment pour pouvoir lui passer les informations du User connecter
                                        fragmentAtDisplay    =   ProfilFragment.newInstance((User) getIntent().getSerializableExtra(USER_CONNECT));
                                        // Ajout du Fragment a l'activity
                                        getSupportFragmentManager()
                                                .beginTransaction()
                                                .replace(R.id.fragment_replace, fragmentAtDisplay)
                                                .commit();
                                        return true;
        }

        return false;
    }



    /** Interface des Fragments **/
    @Override
    public void onFragmentInteraction(Uri uri) { }


// Fin de la Class
}
