package com.example.gabrielgaggini.wyo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import de.hdodenhof.circleimageview.CircleImageView;


public class AddStatusFragment extends Fragment implements DataSendServer
{
    /** Attributs de la Class **/
    private static  final       String          USER_CONNECT            =   "user_connect";
    private         final       int             PICK_IMAGE_REQUEST      =   1;
    private                     ImageView       image_add_statuts;
    private                     EditText        content_add;
    private                     CircleImageView button_getImage;
    private                     TextView        pseudoAuteur;
    private                     CircleImageView imageUser;
    private                     Button          button_send_statuts;
    private                     OnFragmentInteractionListener mListener;
    private                     String          imageBase64;





    /** Constructeur du Fragment **/
    public AddStatusFragment() { }




    /** Constructeur static du Fragment **/
    public static AddStatusFragment newInstance(User userConnect)
    {
        AddStatusFragment   fragment    =   new AddStatusFragment();

        Bundle              args        =   new Bundle();
        args.putSerializable(USER_CONNECT, userConnect);

        fragment.setArguments(args);

        return fragment;
    }




    /** Methode qui crée le Fragment **/
    @Override public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
            if (getArguments() != null) { String mParam1 = getArguments().getString(USER_CONNECT); }
    }




    /** Methode qui va monter le Fragment aux yeux du User **/
    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Monte le Fragment a l'ecran
        View view               =   inflater.inflate(R.layout.add_statuts_fragment, container, false);

        // Récupere les éléments
        this.imageUser              =   (CircleImageView)   view.findViewById(R.id.image_user_add);
        this.pseudoAuteur           =   (TextView)          view.findViewById(R.id.pseudo_user_add);
        this.image_add_statuts      =   (ImageView)         view.findViewById(R.id.image_content_statuts);
        this.content_add            =   (EditText)          view.findViewById(R.id.contenu_statuts_add);
        this.button_getImage        =   (CircleImageView)   view.findViewById(R.id.button_get_image_add);
        this.button_send_statuts    =   (Button)            view.findViewById(R.id.button_add_statuts);

        // Je met le boutons en avant de tous
        this.button_getImage.bringToFront();


        // Cick du boutons pour selectionner une image
        this.button_getImage.setOnClickListener(this.onClickListener);
        this.button_send_statuts.setOnClickListener(this.onClickListener);

        // Verifie que les aruguments ne sont pas null
        if(getArguments() != null)
        {
            // Récupere les User passer en Argumens
            User    userConnect     =   (User) getArguments().getSerializable(USER_CONNECT);

                // Verifie que le User n'est pas null
                if(userConnect != null)
                {
                    // Ajout du pseudo au profil dans le TextView
                    this.pseudoAuteur.setText(userConnect.getuPseudo());

                    // Ajout de l'image de Profil
                    Picasso.get()
                            .load(userConnect.getuImageUrl())
                            .into(this.imageUser);
                }
        }

        return view;
    }



    /** Objet de type listener pour le click des boutons **/
    View.OnClickListener onClickListener = new View.OnClickListener()
    {
        @Override public void onClick(View v)
        {
            switch (v.getId())
            {
                case R.id.button_get_image_add:
                                                selectImageFromGallery();
                                                break;

                case R.id.button_add_statuts:
                                                sendStatutsToServer();
                                                break;
            }
        }
    };



    /** Methode qu ivérifie que le champs n'est pas vide **/
    private Boolean contenuTextIsEmpty()
    {
        if(this.content_add.getText().toString().trim().length() < 1)
        {
            this.content_add.setError("Veuillez remplir le champs");
            return false;
        }
        return true;
    }



    /** Methode qui va envoyer les données du Statuts au serveur **/
    private void sendStatutsToServer()
    {
        if(this.contenuTextIsEmpty())
        {
            JSONObject  jsonObjectStatuts   =   new JSONObject();

            try
            {
                // Verifie que les arguments ne sont pas vide
                if(getArguments() != null)
                {
                    // Récupere du User connecter
                    User    userConnect =   (User) getArguments().getSerializable(USER_CONNECT);

                        // Verifie que le User n'est pas vide
                        if(userConnect != null)
                        {
                            // Ajout des données au JSON
                            jsonObjectStatuts
                                    .put("textContenu", this.content_add.getText().toString());
                            jsonObjectStatuts
                                    .put("imageBase64", this.imageBase64);
                            jsonObjectStatuts
                                    .put("uId", userConnect.getuId());
                            jsonObjectStatuts
                                    .put("cId", 1);

                                // Envoi des données
                                try { ContentManager.sendStatutsRequest(this, jsonObjectStatuts); }
                                catch (UnsupportedEncodingException e) { e.printStackTrace(); }
                        }
                        else { Log.e("sendStatutsToServer", "Le User est vide"); }
                }
                else { Log.e("sendStatutsToServer", "Les arguments sont vide"); }

            }
            catch (JSONException e) { e.printStackTrace(); }
        }
    }





    /** Methode qui va crée un Intent pour récuperer l'image dans la galery **/
    private void selectImageFromGallery()
    {
        Intent intent   =   new Intent();

        // Montre uniquement les images du téléphone
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        // Start de l'activity onActivityOnResult
        startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
    }




    /** Methode qui va récuperer le lien de l'image et qui va la charger dans l'image View **/
    @Override public void onActivityResult(int reqCode, int resCode, Intent data)
    {
        Bitmap bitmap = null;

        if(resCode == Activity.RESULT_OK && data != null)
        {
            // J'essaye de charger l'image a partir de l'URL envoyer par l'intent data
            try
            {
                if(getContext() != null && data.getData() != null)
                {
                    // Recupere l'image du lien renvoyer par l'intent Data
                    bitmap  = BitmapFactory.decodeStream(getContext().getContentResolver().openInputStream(data.getData()));
                    // Ajout de l'image a l'image View
                    this.image_add_statuts.setImageBitmap(bitmap);
                    // Change la visibilité de l'image
                    this.image_add_statuts.setVisibility(View.VISIBLE);

                    ByteArrayOutputStream byteArrayOutputStream =   new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    // Tableau qui va contenir l'image en charactère
                    byte[] bytes    =   byteArrayOutputStream.toByteArray();
                    // Création de l'image encode en BASE 64
                    this.imageBase64    = Base64.encodeToString(bytes, Base64.DEFAULT);
                }
                else
                {
                    Log.e("AddStatus", "Le Context est null");
                }

            }
            catch (Exception e) { e.printStackTrace(); }


        }

    }





    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) { if (mListener != null) {  mListener.onFragmentInteraction(uri); } }




    /** Methode appeler a l'attachement du Fragment **/
    @Override public void onAttach(Context context)
    {
        super.onAttach(context);

        if (context instanceof OnFragmentInteractionListener) { mListener = (OnFragmentInteractionListener) context; }
        else { throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener"); }
    }





    @Override public void onDetach()
    {
        super.onDetach();
        mListener = null;
    }




    /** Methode appeler par le requete async lorsque les données sont envoyer au serveur **/
    @Override public void dataSendSuccess(String serverReponse)
    {
        // On receptionne la response du serveur
        try
        {
            JSONObject  jsonObject  =   new JSONObject(serverReponse);
            JSONArray   jsonArray   =   jsonObject.optJSONArray("statuts");

            // Crée un custom Toast
            Toast       toast       =   new Toast(getContext());
                        toast
                            .setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                        toast
                            .setView(new CustomContentDialog(getContext(), jsonArray.getJSONObject(0).getString("content")));
                        toast
                            .show();
        }
        catch (JSONException e) { e.printStackTrace(); }


    }

    @Override public void dataSendFailed() { Toast.makeText(getContext(), "Failed Send With Success", Toast.LENGTH_LONG).show();}





    public interface OnFragmentInteractionListener { void onFragmentInteraction(Uri uri); }

// Fin de la Class
}
