package com.example.gabrielgaggini.wyo;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class HomeFragment extends Fragment implements DataStatusListener, RecylerClickListener
{
    /** Attributs de la Class **/
    private static  final       String                          TAG                 =   "HOME_FRAGMENT";
    private static  final       String                          USER_CONNECT        =   "user_connect";
    private                     OnFragmentInteractionListener   mListener;
    private                     RecyclerView                    recyclerView;
    private                     RecyclerViewAdapter             adapter;
    private                     FrameLayout                     progressBar;
    private                     SwipeRefreshLayout              swipeRefreshLayout;
    private                     TextView                        emptyStatuts;


    /** Constructeur appeler lors de la création d'un Fragment **/
    public HomeFragment() { }


    /** Constructeur static de la class **/
    public static HomeFragment newInstance(User userConnect)
    {
        HomeFragment    fragment    =   new HomeFragment();
        Bundle          args        =   new Bundle();

        args.putSerializable(USER_CONNECT, userConnect);

        fragment.setArguments(args);

        return fragment;
    }



    /** Methode de la création de vue Fragment **/
    @Override public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

            if (getArguments() != null)
            {
                String mParam1 = getArguments().getString(USER_CONNECT);
            }
    }




    /** Methode qui va monter la vue sur l'ecran **/
    @Override public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Recupere le vue correspondante
        View homeFragment       =   inflater.inflate(R.layout.home_fragment, container, false);

        User userConnect        =   (User) getArguments().getSerializable(USER_CONNECT);

        // Récupere les ID des elements
        this.progressBar        =   homeFragment.findViewById(R.id.progress_bar);
        this.recyclerView       =   homeFragment.findViewById(R.id.recyler_home);
        this.swipeRefreshLayout =   homeFragment.findViewById(R.id.swipe_refresh);
        this.emptyStatuts       =   homeFragment.findViewById(R.id.home_empty_statuts);

        // Création de l'adapter pour le recycler view
        this.adapter            =   new RecyclerViewAdapter( this, new StatusCustomCellView(getContext()),userConnect.getuId());

        // Affichage de la progress bar
        this.progressBar.setVisibility(View.VISIBLE);

        this.swipeRefreshLayout.setOnRefreshListener(this.onRefreshListener);

        // Ajout de l'adapter
        this.recyclerView.setAdapter(adapter);

        // Ajout des parametre de l'affichage de la recycler view
        this.recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        // Lancement de la requete vers le serveur qui contient le JSON
        ContentManager.getStatutsRequest(HomeFragment.this);

        // Renvoi le fragement crée
        return homeFragment;
    }





    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) { if (mListener != null) { mListener.onFragmentInteraction(uri); } }



    /** Methode appeler au moment de l'attachement d'un Fragment **/
    @Override public void onAttach(Context context)
    {
        super.onAttach(context);
            if (context instanceof OnFragmentInteractionListener) { mListener = (OnFragmentInteractionListener) context; }
                else { throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener"); }
    }



    @Override public void onDetach()
    {
        super.onDetach();
        mListener = null;
    }


    /** Methode executer lorsque que l'on swipe pour refresh **/
    SwipeRefreshLayout.OnRefreshListener onRefreshListener  =   new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh()
        {
            HomeFragment.this.swipeRefreshLayout.setRefreshing(true);
            ContentManager.getStatutsRequest(HomeFragment.this);
        }
    };





    /** Methode de l'interface appeler si les status sont bien récuperer **/
    @Override public void dataSucessRetrieved(ArrayList<Status> articles)
    {
        this.adapter.adapterRefreshListArticle(articles);

            if(this.getActivity() != null)
            {
                this.progressBar.setVisibility(View.GONE);
                this.swipeRefreshLayout.setRefreshing(false);
            }

        this.progressBar.setVisibility(View.GONE);
    }



    /** Methode appeler si les status ne osnt pas récuperer **/
    @Override public void dataNotSucessRetrieved()
    {
        // On verifie que le context ne sois pas null
        if(this.getActivity() != null)
        {
            Toast.makeText(this.getActivity(), "Not Sucess", Toast.LENGTH_SHORT).show();
        }
    }



    /** Methode qui va permettre les Click sur la RecyclerView **/
    @Override public void onClickItemRecycler(int position, List<Status> statutsList)
    {
        // Generation d'un ID pour le statuts
        String  id  =   "" + Math.random();

        // Ajout de la liste avec un id et la liste des statuts
        ContentHolder.getInstance().setStatutsList(id, statutsList);

        // Récupere le statuts qui nous interesse
        Status  statusClick  =   ContentHolder.getInstance().getStatuts(id).get(position);

        // On clear la liste des status du ContentHolder
        ContentHolder.getInstance().cleanStatuts(id);

        // Chargement de la nouvelle activity en lui passant le statuts qui vient d'etre clicker
        startActivity(new Intent(getContext(), DetailStatut.class).putExtra("statuts_click", statusClick));
    }



    /** Les activité qui vont utiliser se fragment doivent implémenter cette interface **/
    public interface OnFragmentInteractionListener { void onFragmentInteraction(Uri uri); }


// Fin de la Class du Fragment
}
