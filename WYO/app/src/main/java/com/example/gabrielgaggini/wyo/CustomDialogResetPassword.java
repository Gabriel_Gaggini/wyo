package com.example.gabrielgaggini.wyo;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

public class CustomDialogResetPassword extends LinearLayout
{
    /** Attributs de la Class **/
    private EditText    pseudo_forget;
    private EditText    password_forget;


    /** Constrcuteur de la Class **/
    public CustomDialogResetPassword(Context context)
    {
        super(context);
        this.getIdOfElement(context);
    }

    /** Constrcuteur de la Class **/
    public CustomDialogResetPassword(Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);
        this.getIdOfElement(context);
    }

    /** Constrcuteur de la Class **/
    public CustomDialogResetPassword(Context context, @Nullable AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        this.getIdOfElement(context);
    }


    /** Methode qui reucpere les éléments de ma vue **/
    private void getIdOfElement(Context asContext)
    {
        View custom = LayoutInflater.from(asContext).inflate(R.layout.activity_custom_dialog_reset_password, this, true);
    }

// Fin de la Class
}
