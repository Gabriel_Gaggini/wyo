package com.example.gabrielgaggini.wyo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class User implements Serializable
{
    /** Attributs de la Class **/
    private int     uId;
    private String  uPseudo;
    private String  uNom;
    private String  uPrenom;
    private String  uMail;
    private String  uDateNaissance;
    private String  uSexe;
    private String  uDateInscription;
    private String  uHeureInscription;
    private String  uImageUrl;


    /** Constructeur **/
    public User(JSONArray data) throws JSONException
    {
        this.uId                =   data.getJSONObject(0).getInt("uId");
        this.uPseudo            =   data.getJSONObject(0).getString("uPseudo");
        this.uNom               =   data.getJSONObject(0).getString("uNom");
        this.uPrenom            =   data.getJSONObject(0).getString("uPrenom");
        this.uMail              =   data.getJSONObject(0).getString("uMail");
        this.uDateNaissance     =   data.getJSONObject(0).getString("uDateNaisance");
        this.uSexe              =   data.getJSONObject(0).getString("uSexe");
        this.uDateInscription   =   data.getJSONObject(0).getString("uDateInscription");
        this.uHeureInscription  =   data.getJSONObject(0).getString("uHeureInscription");
        this.uImageUrl          =   data.getJSONObject(0).getString("uImageUrl");
    }


    /** Getters de la Class **/
    public int getuId()                     { return this.uId; }
    public String getuPseudo()              { return this.uPseudo; }
    public String getuNom()                 { return this.uNom; }
    public String getuPrenom()              { return this.uPrenom; }
    public String getuMail()                { return this.uMail; }
    public String getuDateNaissance()       { return this.uDateNaissance; }
    public String getuSexe()                { return this.uSexe; }
    public String getuDateInscription()     { return this.uDateInscription; }
    public String getuHeureInscription()    { return this.uHeureInscription; }
    public String getuImageUrl()            { return this.uImageUrl; }



// Fin de la Class
}
