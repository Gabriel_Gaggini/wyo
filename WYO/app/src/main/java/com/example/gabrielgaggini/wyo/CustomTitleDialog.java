package com.example.gabrielgaggini.wyo;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CustomTitleDialog extends LinearLayout
{

    /** Attributs de la Class **/
    private     static  final   int         LAYOUT_ID   =   R.layout.activity_custom_title_dialog;
    private                     TextView    titleDialog;


    /** Constructeur de la Class **/
    public CustomTitleDialog(Context context, String title)
    {
        super(context);
        this.getIdOfActivityElement(context);
        this.setTitleDialog(title);
    }


    /** Constructeur de la Class **/
    public CustomTitleDialog(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    /** Constructeur de la Class **/
    public CustomTitleDialog(Context context,  AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }



    /** Methode qui va récuperer les élément de l'activity **/
    private void getIdOfActivityElement(Context asContext)
    {
        View customViewCell     = LayoutInflater.from(asContext).inflate(LAYOUT_ID, this, true);
        this.titleDialog        = customViewCell.findViewById(R.id.dialog_title);
    }

    /** Methode qui va inscire le titre dans mon TextView **/
    private void setTitleDialog(String name) { this.titleDialog.setText(name); }
}
