package com.example.gabrielgaggini.wyo;

import java.util.List;

public interface DeleteStatutsClickListener
{
    void onClickToDeleteStatuts(int position, List<Status> statutsList);
}
