package com.example.gabrielgaggini.wyo;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

public class PagerProfil extends FragmentStatePagerAdapter
{
    /** Attributs de la Class **/
    private     int     tabCount;
    private     User    userConnect;



    /** Constructeur **/
    public PagerProfil (FragmentManager fm, int tabCount, User userConnect)
    {
        super(fm);
        this.tabCount       =   tabCount;
        this.userConnect    =   userConnect;
    }




    /** Methode de la Class **/
    @Override public Fragment getItem(int position)
    {
        Fragment fragment_to_display    =   null;

            switch (position)
            {
                case 0:
                    fragment_to_display     =   ProfilInformationUser.newInstance(this.userConnect);
                    return fragment_to_display;
                case 1:
                    fragment_to_display     =   ProfilInformationUser.newInstance(this.userConnect);
                    return fragment_to_display;

                case 2:
                    fragment_to_display     =   ProfilInformationUser.newInstance(this.userConnect);
                    return fragment_to_display;

                default:
                        Log.e("PagerProfil", "Aucun Fragment n'a pu etre crée");
                        return null;
            }
    }



    @Override public int getCount() { return this.tabCount; }


// Fin de la Class
}
