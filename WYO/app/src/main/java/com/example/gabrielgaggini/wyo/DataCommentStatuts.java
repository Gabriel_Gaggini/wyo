package com.example.gabrielgaggini.wyo;

import java.util.ArrayList;

public interface DataCommentStatuts
{
    void commentReceive(ArrayList<Commentaire> arrayListComment);
    void commentNotReceive();
}
