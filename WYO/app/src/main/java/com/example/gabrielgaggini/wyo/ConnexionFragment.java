/** Class qui mermet la connexion de l'utilisateurs
 *  Made By Gabriel Gaggini **/

package com.example.gabrielgaggini.wyo;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;



public class ConnexionFragment extends Fragment implements DataSendServer
{
    /** Attributs de la Class **/
    private static final        String              ARG_PARAM1 = "param1";
    private static final        String              ARG_PARAM2 = "param2";
    private                     String              mParam1;
    private                     String              mParam2;
    private                     OnFragmentInteractionListener mListener;
    private                     EditText            pseudo_connexion;
    private                     EditText            password_connexion;
    private                     Button              button_connexion;
    private                     TextView            button_change_password;

    /** Variable néccesaire au ThreadPoolExecutor **/
    private static              int                 NUMBER_OF_CORE           =      Runtime.getRuntime().availableProcessors();
    private static  final       int                 KEEP_ALIVE_TIME          =      1;
    private static  final       TimeUnit            KEEP_ALIVE_TIME_UNIT     =      TimeUnit.SECONDS;
    private static              ThreadPoolExecutor  EXECUTOR                 =      new ThreadPoolExecutor( NUMBER_OF_CORE,
                                                                                                            NUMBER_OF_CORE,
                                                                                                            KEEP_ALIVE_TIME,
                                                                                                            KEEP_ALIVE_TIME_UNIT,
                                                                                                            new PriorityBlockingQueue<Runnable>(1));




    /** Constructeur **/
    public ConnexionFragment() { }




    /** Constructeur 2 **/
    private static ConnexionFragment newInstance(String param1, String param2)
    {
        ConnexionFragment   fragment    = new ConnexionFragment();
        Bundle args        = new Bundle();

        args
                .putString(ARG_PARAM1, param1);
        args
                .putString(ARG_PARAM2, param2);
        fragment
                .setArguments(args);

        return fragment;
    }




    @Override public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (getArguments() != null)
        {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    /** Methode qui crée la vue **/
    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View    connexion_fragment  =   inflater.inflate(R.layout.connexion_fragment, container, false);

        this.getIdOfElement(connexion_fragment);

        return connexion_fragment;
    }




    /** Methode qui va récuerer l'ID des element du Fragment **/
    @SuppressLint("ClickableViewAccessibility")
    private void getIdOfElement(View connexion_fragment)
    {
        this.pseudo_connexion       =   connexion_fragment.findViewById(R.id.connexion_pseudo);
        this.password_connexion     =   connexion_fragment.findViewById(R.id.connexion_password);
        this.button_connexion       =   connexion_fragment.findViewById(R.id.button_connexion);
        this.button_change_password =   connexion_fragment.findViewById(R.id.button_connexion_forget_password);

        // Ajout des Listener
        this.button_connexion.setOnClickListener(this.onClickListener);
        this.button_change_password.setOnClickListener(this.onClickListener);
        this.pseudo_connexion.setOnTouchListener(this.onTouchListener);
        this.password_connexion.setOnTouchListener(this.onTouchListener);
    }



    /** Methode qui vérifie que le pseudo est bien renseigner **/
    private Boolean isEmptyPseudo()
    {
        if(this.pseudo_connexion.getText().toString().trim().length() < 4)
        {
            this.pseudo_connexion.setBackgroundResource(R.drawable.edit_text_error);
            this.pseudo_connexion.setHintTextColor(Color.RED);
            this.pseudo_connexion.setTextColor(Color.RED);
            this.pseudo_connexion.setError("Le pseudo doit etre renseigner");

            return false;
        }

        // Sinon je note que tout est bon
        this.pseudo_connexion.setBackgroundResource(R.drawable.edit_text_valid);
        this.pseudo_connexion.setTextColor(Color.parseColor("#009f03"));

        return true;
    }


    /** Methode qui vérifie que le password est bien renseigner **/
    private Boolean isEmptyPassword()
    {
        if(this.password_connexion.getText().toString().trim().length() < 4)
        {
            this.password_connexion.setBackgroundResource(R.drawable.edit_text_error);
            this.password_connexion.setHintTextColor(Color.RED);
            this.password_connexion.setTextColor(Color.RED);
            this.password_connexion.setError("Le mot de passe doit etre rensigner");

            return false;
        }

        // Sinon je note que tout est bon
        this.password_connexion.setBackgroundResource(R.drawable.edit_text_valid);
        this.password_connexion.setTextColor(Color.parseColor("#009f03"));

        return true;
    }




    /** Listener pour les click des boutons **/
    View.OnClickListener onClickListener    =   new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            switch (view.getId())
            {
                case R.id.button_connexion:
                    connexionToTheApp();
                    break;

                case R.id.button_connexion_forget_password:
                    Utils.createCustoDialogResetPassword(getContext());
                    break;
            }
        }
    };


    /** Création d'un objet au focus des edit text **/
    View.OnTouchListener onTouchListener    =   new View.OnTouchListener()
    {
        @Override public boolean onTouch(View view, MotionEvent motionEvent)
        {
            switch (view.getId())
            {
                case R.id.connexion_pseudo:
                    pseudo_connexion.setTextColor(Color.BLACK);
                    pseudo_connexion.setHintTextColor(Color.GRAY);
                    return false;

                case R.id.connexion_password:
                    password_connexion.setTextColor(Color.BLACK);
                    password_connexion.setHintTextColor(Color.GRAY);
                    return false;
            }
            return false;
        }
    };



    /** Methode qui va permettre la connexion **/
    private void connexionToTheApp()
    {
        JSONObject jsonDataUser    =   null;

            try
            {
                // Verifie que les champs ne sont pas vide
                if(this.isEmptyPseudo() && this.isEmptyPassword())
                {
                    jsonDataUser    =   new JSONObject();
                    // Ajout des données au JSON
                    jsonDataUser
                            .put("uPseudo", pseudo_connexion.getText().toString());
                    jsonDataUser
                            .put("uPassword", password_connexion.getText().toString());

                    // Requete vers le serveur pour vérifier la connexion
                    ContentManager.sendConnexionRequest(this, jsonDataUser);
                }
            }
            catch (JSONException | UnsupportedEncodingException e) { e.printStackTrace(); }
    }




    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) { if (mListener != null) {  mListener.onFragmentInteraction(uri); } }




    @Override public void onAttach(Context context)
    {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {  mListener = (OnFragmentInteractionListener) context; }
        else { throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener"); }
    }



    @Override public void onDetach()
    {
        super.onDetach();
        mListener = null;
    }




    /** Methode qui va reinitialiser les champs **/
    private void updateView()
    {
        // Reinitialise les champs
        this.pseudo_connexion.setText("");
        this.password_connexion.setText("");
        this.pseudo_connexion.setBackgroundResource(R.drawable.edit_text_custom);
        this.password_connexion.setBackgroundResource(R.drawable.edit_text_custom);
    }




    /** Si les données ont reussi a aboutir sans probleme a leur point données **/
    @Override public void dataSendSuccess(String serverReponse)
    {
        try
        {
            JSONObject  jsonObject  =   new JSONObject(serverReponse);
            JSONArray   jsonArray   =   jsonObject.optJSONArray("user");
            Intent      intent      =   null;

                // Vérifie que la connexion est autorisé
                if(jsonArray.getJSONObject(0).getBoolean("connexion"))
                {
                    User    userConnect         =   new User(jsonArray);

                            intent              =   new Intent(getContext(), MainActivity.class);
                            intent.putExtra("userIsConnect", (Serializable) userConnect);

                    // Demarre la nouvel activity
                    startActivity(intent);

                    this.updateView();
                }
                else
                {
                    Utils.createCustomDialogInformation(jsonArray.getJSONObject(0).getString("content"), "Information", getContext());
                }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.e("dataSendSuces Connexion", "Impossible de parser le JSON");
        }
    }




    /** Si les données n'ont pas reussi a aboutir **/
    @Override public void dataSendFailed() { }




    /** Interface du fragment implmenter par ceux qui l'utilise**/
    public interface OnFragmentInteractionListener { void onFragmentInteraction(Uri uri); }



// Fin de la Class
}
