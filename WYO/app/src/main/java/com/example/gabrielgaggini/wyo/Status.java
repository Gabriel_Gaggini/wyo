/** Class qui modelise mes Status
 *  Made by Gabriel Gaggini **/

package com.example.gabrielgaggini.wyo;

import java.io.Serializable;

public class Status implements Serializable
{
    /** Attributs de la Class **/
    private     int     sId;
    private     String  sContenu;
    private     int     sLike;
    private     String  sHeure;
    private     int     sIsSignaled;
    private     String  sDate;
    private     String  urlProfilImage;
    private     String  urlContenuImage;
    private     int     uId;
    private     int     cId;
    private     String  auteur;
    private     String  categorie;
    private     int     nbComment;



    /** Constructeur de la Class **/
    public Status(int sId, String sContenu, int sLike, String sheure, int sIsSignale, String sDate, String urlProfilI, String urlContenu, int uId, int cId, String auteur, String categorie, int nbComment)
    {
        this.sId                =   sId;
        this.sContenu           =   sContenu;
        this.sLike              =   sLike;
        this.sHeure             =   sheure;
        this.sIsSignaled        =   sIsSignale;
        this.sDate              =   sDate;
        this.urlProfilImage     =   urlProfilI;
        this.urlContenuImage    =   urlContenu;
        this.uId                =   uId;
        this.cId                =   cId;
        this.auteur             =   auteur;
        this.categorie          =   categorie;
        this.nbComment          =   nbComment;
    }



    /** Getters des Attributs **/
    public  int      getsId()              { return sId; }
    public  String   getsContenu()         { return sContenu; }
    public  int      getsLike()            { return sLike; }
    public  String   getsHeure()           { return sHeure; }
    public  int      getsIsSignaled()      { return sIsSignaled; }
    public  String   getsDate()            { return sDate; }
    public  String   getUrlProfilImage()   { return urlProfilImage; }
    public  String   getUrlContenuImage()  { return urlContenuImage; }
    public  int      getuId()              { return uId; }
    public  int      getcId()              { return cId; }
    public  String   getAuteur()           { return auteur; }
    public  String   getCategorie()        { return categorie; }
    public  int      getNbComment()        { return nbComment; }


// Fin de la Class
}
