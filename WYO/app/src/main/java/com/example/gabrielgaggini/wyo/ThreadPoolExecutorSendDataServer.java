package com.example.gabrielgaggini.wyo;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import org.json.JSONObject;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class ThreadPoolExecutorSendDataServer implements Runnable, Comparable
{
    /** Attributs de la Class **/
    private         String          url;
    private         DataSendServer  dataSendServer;
    private final   Handler         mMainHandler    = new Handler(Looper.getMainLooper());
    private         String          jsonToSend;



    /** Constructeur **/
    public ThreadPoolExecutorSendDataServer(String url, DataSendServer dataSendServer, JSONObject jsonObject) throws UnsupportedEncodingException {
        this.url            =   url;
        this.dataSendServer =   dataSendServer;
        // J'encode le JSON en utf-8
        this.jsonToSend     =   URLEncoder.encode(jsonObject.toString(), "UTF-8");
    }


    /** Methode qui s'execute lorsque je lance une requete **/
    @Override public void run()
    {
        HttpURLConnection httpURLConnection = null;

        try
        {
            if(!Utils.isValidString(this.url))
            {
                return;
            }

            // Url du serveur
            URL     url             =   new URL(this.url);

            // Ouvre la connexion avec le serveur
            httpURLConnection       =   (HttpURLConnection) url.openConnection();
            // J'anonce que cela va etre une requete POST
            httpURLConnection.setRequestMethod("POST");
            // Ajout d'un temps d'attente maximum
            httpURLConnection.setConnectTimeout(10000);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.connect();


            // Objet qui permet d'envoyer les données
            DataOutputStream    wr     =   new DataOutputStream(httpURLConnection.getOutputStream());


            // Ajout des données
            wr.writeBytes("userData="+ jsonToSend);

            wr.flush();
            wr.close();

                // Vérifie le status de la requetes
                if(httpURLConnection.getResponseCode() != 200)
                {
                    this.dataDoNotSendToServer();
                    return;
                }


            // Récupere la data afficher a l'ecran
            InputStream inputStream =   httpURLConnection.getInputStream();

            // J'extrait le tout en String
            String      reponse     =   Utils.convertJsonOnString(inputStream);

                // verifie que la reponse n'est pas null
                if(reponse != null)
                {
                    this.dataSendToServer(reponse);
                }


        } catch (IOException e) { e.printStackTrace(); }
    }




    /** Methode qui sert a rien **/
    @Override public int compareTo(@NonNull Object o) { return 0; }


    /** Methode appeler pour passer l'information au fragment concerné **/
    private void dataSendToServer(final String jsonReponse)
    {
        final DataSendServer dataSendServer = this.dataSendServer;

            if (dataSendServer != null)
            {
                mMainHandler.post(new Runnable()
                {
                    @Override
                    public void run() { dataSendServer.dataSendSuccess(jsonReponse); }
                });
            }
    }



    /** Methode appeler si un problème est survenu **/
    private void dataDoNotSendToServer()
    {
        final DataSendServer dataSendServer = this.dataSendServer;

            if (dataSendServer != null)
            {
                mMainHandler.post(new Runnable()
                {
                    @Override
                    public void run() { dataSendServer.dataSendFailed(); }
                });
            }
    }


// Fin de la Class
}
