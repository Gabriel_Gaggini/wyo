/** Class qui va inscrire les users
 *  Made By Gabriel Gaggini
 */

package com.example.gabrielgaggini.wyo;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import de.hdodenhof.circleimageview.CircleImageView;


public class InscriptionFragment extends Fragment implements DataSendServer
{
    /** Attributs de la Class**/
    private static  final       String          ARG_PARAM1          =   "param1";
    private static  final       String          ARG_PARAM2          =   "param2";
    private         final       int             LAYOUT_FRAGMENT     =   R.layout.inscription_fragment;
    private         final       int             PICK_IMAGE_REQUEST  =   1;
    private                     String          mParam1;
    private                     String          mParam2;
    /** Attributs du Fragment **/
    private                     EditText        pseudo;
    private                     EditText        password;
    private                     EditText        nom;
    private                     EditText        prenom;
    private                     EditText        email;
    private                     EditText        dateNaissance;
    private                     RadioGroup      sexeGroup;
    private                     RadioButton     radioSexeButton;
    private                     Button          buttonCreateUser;
    private                     String          date_naissance_parse;
    private                     ProgressBar     progressBar;
    private                     CircleImageView circleImageView;
    private                     String          imageProfilBase64;
    /** Listener du Fragment **/
    private                     OnFragmentInteractionListener mListener;




    /** Constructeur **/
    public InscriptionFragment() { }



    /** Constructeur 2 **/
    public static InscriptionFragment newInstance(String param1, String param2)
    {
        InscriptionFragment fragment    = new InscriptionFragment();
        Bundle              args        = new Bundle();

        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);

        fragment.setArguments(args);

        return fragment;
    }




    @Override public void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);

            if (getArguments() != null)
            {
                mParam1 = getArguments()
                        .getString(ARG_PARAM1);
                mParam2 = getArguments()
                        .getString(ARG_PARAM2);
            }
    }





    /** Methode qui va crée la vue du Fragment **/
    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Monte la vue
        View    inscrptionFragment  =   inflater.inflate(LAYOUT_FRAGMENT, container, false);

        // Récupere les elements du fragment
        this.getIdOfElement(inscrptionFragment);

        // Renvoi le fragment crée
        return inscrptionFragment;
    }





    /** Création d'un listener qu va écouter les click des boutons **/
    View.OnClickListener    onClickListener =   new View.OnClickListener()
    {

        @Override public void onClick(View view)
        {
            switch (view.getId())
            {
                case R.id.button_inscription :
                    // Envoi des information a la base de données
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.bringToFront();

                    try { sendDataUser(); }
                    catch (UnsupportedEncodingException e) { e.printStackTrace(); }
                    break;


                case R.id.inscription_picture :
                    // Selectionne la photo dans la galerie
                    selectImageFromGallery();
                    break;

            }

        }
    };






    /** Methode pour revenir en arrière **/
    public void onButtonPressed(Uri uri) { if (mListener != null) { mListener.onFragmentInteraction(uri); } }



    /** Methode appeler au moment de l'attachement d'un Fragment **/
    @Override public void onAttach(Context context)
    {
        super.onAttach(context);
            if (context instanceof OnFragmentInteractionListener) {  mListener = (OnFragmentInteractionListener) context; }
                else { throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener"); }
    }





    @Override public void onDetach()
    {
        super.onDetach();
        mListener = null;
    }





    /** Methode qui va récuperer les ID des différents éléments **/
    @SuppressLint("ClickableViewAccessibility")
    private void getIdOfElement(View inscriptionView)
    {
        // Récupere les ID de mes éléments
        this.pseudo             =   inscriptionView.findViewById(R.id.inscription_pseudo);
        this.password           =   inscriptionView.findViewById(R.id.inscription_password);
        this.nom                =   inscriptionView.findViewById(R.id.inscription_nom);
        this.prenom             =   inscriptionView.findViewById(R.id.inscription_prenom);
        this.email              =   inscriptionView.findViewById(R.id.inscription_mail);
        this.sexeGroup          =   inscriptionView.findViewById(R.id.inscription_sexe);
        this.dateNaissance      =   inscriptionView.findViewById(R.id.inscription_date_naissance);
        this.buttonCreateUser   =   inscriptionView.findViewById(R.id.button_inscription);
        this.progressBar        =   inscriptionView.findViewById(R.id.progress_bar);
        this.circleImageView    =   inscriptionView.findViewById(R.id.inscription_picture);

        // Masque la Progress Bar
        this.progressBar.setVisibility(View.GONE);


        // Ajout d'un Touch Listener pour change certaine couleur
        this.pseudo
                .setOnTouchListener(this.onTouchListener);
        this.password
                .setOnTouchListener(this.onTouchListener);
        this.nom
                .setOnTouchListener(this.onTouchListener);
        this.prenom
                .setOnTouchListener(this.onTouchListener);


        // Ajout d'un CLick Listener
        this.buttonCreateUser
                .setOnClickListener(this.onClickListener);
        this.circleImageView
                .setOnClickListener(this.onClickListener);

    }


    /** Création d'un objet au focus des edit text **/
    View.OnTouchListener onTouchListener    =   new View.OnTouchListener()
    {
        @Override public boolean onTouch(View view, MotionEvent motionEvent)
        {
            switch (view.getId())
            {
                case R.id.inscription_pseudo:
                    pseudo.setTextColor(Color.BLACK);
                    pseudo.setHintTextColor(Color.GRAY);
                    return false;

                case R.id.inscription_password:
                    password.setTextColor(Color.BLACK);
                    password.setHintTextColor(Color.GRAY);
                    return false;

                case R.id.inscription_nom:
                    nom.setTextColor(Color.BLACK);
                    nom.setHintTextColor(Color.GRAY);
                    return false;

                case R.id.inscription_prenom:
                    prenom.setTextColor(Color.BLACK);
                    prenom.setHintTextColor(Color.GRAY);
                    return false;

                case R.id.inscription_mail:
                    email.setTextColor(Color.BLACK);
                    email.setHintTextColor(Color.GRAY);
                    return false;
            }
            return false;
        }
    };





    /** Methode qui va récuperer les valeurs des éléments **/
    private JSONObject getValuesOfElement()
    {
        JSONObject  jsonDataUser    =   null;

        // On test pour voir si tout se passe bien
        if(this.isValidInscription())
        {
            try
            {
                // Création d'un JSON OBJECT, ces ce JSON qui va etre envoyer a l'API
                jsonDataUser    =   new JSONObject();
                // Inscrit les données dans le json
                jsonDataUser
                        .put("userPseudo", this.pseudo.getText().toString());
                jsonDataUser
                        .put("userPassword", this.password.getText().toString());
                jsonDataUser
                        .put("userNom", this.nom.getText().toString());
                jsonDataUser
                        .put("userPrenom", this.prenom.getText().toString());
                jsonDataUser
                        .put("userMail", this.email.getText().toString());
                jsonDataUser
                        .put("userDateNaissance", this.date_naissance_parse);
                jsonDataUser
                        .put("userSexe", this.getDataOfRadioButton());
                jsonDataUser
                        .put("userImageProfil", this.imageProfilBase64);

            }
            catch (Exception e) { Log.e("getValueOfElement", "Probleme de JSON"); }
        }
        else
        {
            Log.w("getValueOfElement", "Il manque des Information");
        }

        // Renvoi le JSON
        return jsonDataUser;
    }






    /** Methode qui vérifie que les champs sont tous rempli **/
    private Boolean isValidInscription()
    {
        // Si une des données n'est pas bonne on arrete le processus d'ajout
        if      (!this.isValidPseudo())         { return false; }
        else if (!this.isValidPassword())       { return false; }
        else if (!this.isValidNom())            { return false; }
        else if (!this.isValidPrenom())         { return false; }
        else if (!this.isValidEmail())          { return false; }
        else if (!this.isValidDateNaissance())  { return false; }
        else return this.buttonRadioIsValid();
    }





    /** Methode qui vérifie si le pseudo est rentré correctement **/
    @SuppressLint("ResourceAsColor")
    private Boolean isValidPseudo()
    {
        if(this.pseudo.getText().toString().trim().length() < 4)
        {
            this.pseudo.setBackgroundResource(R.drawable.edit_text_error);
            this.pseudo.setHintTextColor(Color.RED);
            this.pseudo.setTextColor(Color.RED);
            this.pseudo.setError("Le pseudo doit contenir au moins 4 caractères");

            return false;
        }

        // Sinon je note que tout est bon
        this.pseudo.setBackgroundResource(R.drawable.edit_text_valid);
        this.pseudo.setTextColor(Color.parseColor("#009f03"));

        return true;

    }





    /** Methode qui va vérifier si le mot de passe est rentré correctement **/
    @SuppressLint("ResourceAsColor")
    private Boolean isValidPassword()
    {
        if(this.password.getText().toString().trim().length() < 4)
        {
            this.password.setBackgroundResource(R.drawable.edit_text_error);
            this.password.setError("Le mot de passe doit contenir 4 caratcères");
            this.password.setTextColor(Color.RED);
            this.password.setHintTextColor(Color.RED);

            return false;
        }

        // Sinon je note que tout est bon
        this.password.setBackgroundResource(R.drawable.edit_text_valid);
        this.password.setTextColor(Color.parseColor("#009f03"));

        return true;
    }





    /** Methode qui va vérifier que le nom est bien renseigner **/
    @SuppressLint("ResourceAsColor")
    private Boolean isValidNom()
    {
        if(this.nom.getText().toString().trim().length() == 0)
        {
            this.nom.setBackgroundResource(R.drawable.edit_text_error);
            this.nom.setHintTextColor(Color.RED);
            this.nom.setError("Le nom est obligatoire");

            return false;
        }

        // Sinon je note que tout est bon
        this.nom.setBackgroundResource(R.drawable.edit_text_valid);
        this.nom.setTextColor(Color.parseColor("#009f03"));

        return true;
    }




    /** Methode qui va vérifier que le prenom est bien renseigner **/
    @SuppressLint("ResourceAsColor")
    private Boolean isValidPrenom()
    {
        if(this.prenom.getText().toString().trim().length() == 0)
        {
            this.prenom.setBackgroundResource(R.drawable.edit_text_error);
            this.prenom.setHintTextColor(Color.RED);
            this.prenom.setError("Le prenom est obligatoire");

            return false;
        }

        // Sinon je note que tout est bon
        this.prenom.setBackgroundResource(R.drawable.edit_text_valid);
        this.prenom.setTextColor(Color.parseColor("#009f03"));

        return true;

    }





    /** Methode qui va vérifier que le mail est bien composer **/
    @SuppressLint("ResourceAsColor")
    private Boolean isValidEmail()
    {
        // Vérifie que l'email n'est pas vide et qu'il respecte la sructure d'un email
        if(this.email.getText().toString().trim().length() > 0 && this.email.getText().toString().matches("[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"))
        {
            this.email.setBackgroundResource(R.drawable.edit_text_valid);
            this.email.setTextColor(Color.parseColor("#009f03"));

            return true;
        }
        else
        {
            this.email.setBackgroundResource(R.drawable.edit_text_error);
            this.email.setError("Exemple: exemple@gmail.com");
            this.email.setHintTextColor(Color.RED);

            return false;
        }
    }





    /** Methode qui va vérifier l'age de la personne et que la date de naisance soit bien formé **/
    @SuppressLint({"ResourceAsColor", "SimpleDateFormat"})
    private Boolean isValidDateNaissance()
    {
        if(this.dateNaissance.getText().toString().trim().length()  <   10)
        {
            this.dateNaissance.setError("Exemple : 28.11.1997");
            this.dateNaissance.setBackgroundResource(R.drawable.edit_text_error);
            return false;
        }
        else
        {
            this.dateNaissance.setBackgroundResource(R.drawable.edit_text_valid);
            this.dateNaissance.setTextColor(Color.parseColor("#009f03"));

            @SuppressLint("SimpleDateFormat")
            DateFormat dateFormat   =   new SimpleDateFormat("dd.MM.yyyy");
            Date dateObject         =   null;

            // Récupere la date
            try
            {
                // Récupere la date de naissance
                dateObject      =   dateFormat.parse(this.dateNaissance.getText().toString());

                // Récupere la date bien formater
                this.date_naissance_parse     =   new SimpleDateFormat("dd/MM/yyyy").format(dateObject);

            }
            catch (java.text.ParseException e)
            {
                e.printStackTrace();
            }

            // TODO Verifier l'age de la personne qui s'inscrit pour voir si on peut la laisser s'inscrire

            return true;
        }


    }


    /** Methode qui va récupere la valeur de mes boutons Radio **/
    private Boolean buttonRadioIsValid()
    {
        // Vérifie que un des deux boutons a été clicker
        if(this.sexeGroup.getCheckedRadioButtonId() == -1)
        {
            this.sexeGroup.setBackgroundResource(R.drawable.edit_text_error);
            return false;
        }

        this.sexeGroup.setBackgroundResource(R.drawable.edit_text_valid);
        return true;
    }



    /** Récupere la valeur des Radio Button **/
    private String getDataOfRadioButton()
    {
        // Regarde lequel des boutons a été séléctionner
        int selectChecked   =   this.sexeGroup.getCheckedRadioButtonId();

        // Récupere le bouton clicker
        this.radioSexeButton    = this.getView().findViewById(selectChecked);

        // Renvoi sa valeur
        return this.radioSexeButton.getText().toString();
    }




    /** Methode qui va lancer la requete pour rentrer les données **/
    private void sendDataUser() throws UnsupportedEncodingException
    {
        // Si l'inscription renvoi false
        if(!this.isValidInscription())
        {
            this.progressBar.setVisibility(View.GONE);
        }
        else
        {
            this.buttonCreateUser.setEnabled(false);
            this.buttonCreateUser.setBackgroundResource(R.drawable.button_shadow);
            ContentManager.sendInscriptionRequest(this, this.getValuesOfElement());

        }
    }





    /** Methode de mon interface pour l'envoi des données au serveur **/
    @Override public void dataSendSuccess(String reponse)
    {
        Utils.createCustomDialogInformation(reponse, "Requete Envoyée",  getContext());
        this.buttonCreateUser.setEnabled(true);
        this.progressBar.setVisibility(View.GONE);
        this.buttonCreateUser.setBackgroundResource(R.drawable.button_shadow);
    }



    @Override public void dataSendFailed()
    {
        Utils.createCustomDialogInformation("Connexion au serveur impossible", "Serveur Hors-Ligne", getContext());
    }



    /** Methode qui va crée un Intent pour récuperer l'image dans la galery **/
    private void selectImageFromGallery()
    {
        Intent intent   =   new Intent();

        // Montre uniquement les images du téléphone
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        // Start de l'activity onActivityOnResult
        startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
    }




    /** Methode qui va récuperer le lien de l'image et qui va la charger dans l'image View **/
    @Override public void onActivityResult(int reqCode, int resCode, Intent data)
    {
        Bitmap bitmap = null;

        // Verifie le résultat de la requete
        if(resCode == Activity.RESULT_OK && data != null)
        {
            // J'essaye de charger l'image a partir de l'URL envoyer par l'intent data
            try
            {
                // Vérifie l'etat du context et le liens de l'image selectionner
                if(getContext() != null && data.getData() != null)
                {
                    // Recupere l'image du lien renvoyer par l'intent Data
                    bitmap  = BitmapFactory.decodeStream(getContext().getContentResolver().openInputStream(data.getData()));
                    // Ajout de l'image a l'image View
                    this.circleImageView.setImageBitmap(bitmap);
                    // Change la visibilité de l'image
                    this.circleImageView.setVisibility(View.VISIBLE);

                    ByteArrayOutputStream byteArrayOutputStream =   new ByteArrayOutputStream();
                    // Compresse l'image
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    // Tableau qui va contenir l'image en charactère
                    byte[] bytes            =   byteArrayOutputStream.toByteArray();
                    // Création de l'image encode en BASE 64
                    this.imageProfilBase64  =   Base64.encodeToString(bytes, Base64.DEFAULT);
                }
                else
                {
                    Log.e("AddStatus", "Le Context est null");
                }

            }
            catch (Exception e) { e.printStackTrace(); }


        }

    }




    /** Interface qui doit etre implémenter par les activité qui s'en serve **/
    public interface OnFragmentInteractionListener { void onFragmentInteraction(Uri uri); }



// Fin de la Class
}
