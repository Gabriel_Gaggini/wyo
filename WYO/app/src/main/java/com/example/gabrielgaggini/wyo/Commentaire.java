/* CLass qui va modeliser les commentaire
 *  Made By Gabriel Gaggini
 */

package com.example.gabrielgaggini.wyo;

public class Commentaire
{
    /** Attributs de la Class **/
    private     int     cId;
    private     String  cContenu;
    private     String  cHeure;
    private     String  cDate;
    private     int     sId;
    private     int     uId;
    private     String  uImageUrl;
    private     String  uPseudo;



    /** Constructeur **/
    public Commentaire(int comId, String comContenu, String comHeure, String comDate, int sId, int uId, String uImageUrl, String uPseudo)
    {
        this.cId        =   comId;
        this.cContenu   =   comContenu;
        this.cHeure     =   comHeure;
        this.cDate      =   comDate;
        this.sId        =   sId;
        this.uId        =   uId;
        this.uPseudo    =   uPseudo;
        this.uImageUrl  =   uImageUrl;
    }



    /** Getters de la Class **/
    public int      getcId()        { return this.cId; }
    public String   getcContenu()   { return this.cContenu; }
    public String   getcHeure()     { return this.cHeure; }
    public String   getcDate()      { return this.cDate; }
    public int      getsId()        { return this.sId; }
    public int      getuId()        { return this.uId; }
    public String   getuImageUrl()  { return this.uImageUrl; }
    public String   getuPseudo()    { return this.uPseudo; }



// Fin de la Class
}
