/** Class qui va me permettre de récuperer un article de la liste
 *  Made By Gabriel Gaggini
 */

package com.example.gabrielgaggini.wyo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ContentHolder
{
    /** Attributs de la Class **/
    private static  ContentHolder               contentHolder_instance;
    private         Map<String, List<Status>>   statutsMap              =   new HashMap<>();


    /** Constructeur **/
    private ContentHolder() {}


    /** Constructeur Static **/
    public static ContentHolder getInstance()
    {
        if(contentHolder_instance == null)
        {
            contentHolder_instance  =   new ContentHolder();
        }

        // Renvoi un objet de type ContentHolder
        return contentHolder_instance;
    }


    /** Ajoute le statuts a la liste **/
    public void setStatutsList(String id, List<Status> statusList) { this.statutsMap.put(id, statusList); }

    /** Recupere un statuts **/
    public List<Status> getStatuts(String id) { return this.statutsMap.get(id); }

    /** Clear la liste **/
    public void cleanStatuts(String id)
    {
        this.statutsMap.put(id, null);
    }
}
