package com.example.gabrielgaggini.wyo;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class RecyclerAdapterComment extends RecyclerView.Adapter<RecyclerAdapterComment.ViewHolder>
{
    /** Attributs de la Class **/
    private ArrayList<Commentaire> arrayListCommentaire =   new ArrayList<>();


    /** Constructeur **/
    public RecyclerAdapterComment() { }




    /** Garde ma vue (Cellule) **/
    public static class ViewHolder extends RecyclerView.ViewHolder { public ViewHolder(View view) { super(view);  }}


    /** Methode de la recylerView **/
    @Override public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View customCellComment  =   new CustomViewComment(parent.getContext());
        // Création des parametres
        RecyclerView.LayoutParams   parametersView  =   new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        // Ajout des parametres a la vue
        customCellComment.setLayoutParams(parametersView);
        return new ViewHolder(customCellComment);

    }



    /** Methode qui va inscire les données dans les vue **/
    @Override public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        final CustomViewComment customViewComment   =   (CustomViewComment) holder.itemView;

        customViewComment.setDataOnView(this.arrayListCommentaire.get(position).getuImageUrl(), this.arrayListCommentaire.get(position).getuPseudo(), this.arrayListCommentaire.get(position).getcContenu());
    }


    /** Methode qui va refresh la liste de Commentaire **/
    public void refreshListComment(ArrayList<Commentaire> arrayListCommentaire)
    {
        this.arrayListCommentaire   =   arrayListCommentaire;
        notifyDataSetChanged();
    }



    @Override
    public int getItemCount() { return this.arrayListCommentaire.size(); }



}
