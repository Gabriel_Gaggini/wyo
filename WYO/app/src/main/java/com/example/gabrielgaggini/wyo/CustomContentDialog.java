package com.example.gabrielgaggini.wyo;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CustomContentDialog extends LinearLayout
{

    /** Attributs de la Class **/
    private     static  final   int         LAYOUT_ID   =   R.layout.activity_custom_content_dialog;
    private                     TextView    dialog_message;


    /** Constructeur **/
    public CustomContentDialog(Context context, String message)
    {
        super(context);
        this.getIdOfActivityElement(context);
        this.setMessage(message);
    }

    /** Constructeur **/
    public CustomContentDialog(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    /** Constructeur **/
    public CustomContentDialog(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }




    /** Methode qui va récuperer les élément de l'activity **/
    private void getIdOfActivityElement(Context asContext)
    {
        View customViewCell  = LayoutInflater.from(asContext).inflate(LAYOUT_ID, this, true);
        this.dialog_message   =   (TextView) customViewCell.findViewById(R.id.message_dialog);
    }




    /** Methode qui permet d'inscire un message dans le TextView **/
    private void setMessage(String message) { this.dialog_message.setText(message); }
}
