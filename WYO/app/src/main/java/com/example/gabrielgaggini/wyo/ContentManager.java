/** Methode qui ve demander les donné a la class qui est charger de les faire
 *  Made by Gabriel Gaggini **/

package com.example.gabrielgaggini.wyo;

import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ContentManager
{
    /** Attributs de la Class **/
    private     static      final   String              TAG                     =       ContentManager.class.getSimpleName();

    // Recupere le nombre de coeur du processeur
    private     static              int                 NUMBER_OF_CORE           =      Runtime.getRuntime().availableProcessors();
    private     static      final   int                 KEEP_ALIVE_TIME          =      1;
    private     static      final   TimeUnit            KEEP_ALIVE_TIME_UNIT     =      TimeUnit.SECONDS;
    private     static              ThreadPoolExecutor  EXECUTOR                 =      new ThreadPoolExecutor( NUMBER_OF_CORE,
                                                                                                                NUMBER_OF_CORE,
                                                                                                                KEEP_ALIVE_TIME,
                                                                                                                KEEP_ALIVE_TIME_UNIT,
                                                                                                                new PriorityBlockingQueue<Runnable>(1));




    /** Constructeur de la Class **/
    private ContentManager() { }




    /** Methode qui va renvoyer un arrayList de statuts et qui va crée les statuts **/
    public static ArrayList<Status> convertJsonOnStatuts(String jsonString)
    {
        if(jsonString != null)
        {
            ArrayList<Status>  statusArrayList    =   new ArrayList<>();

            try
            {
                // Objet du JSON
                JSONObject jsonOnObject        =   new JSONObject(jsonString);

                // Tableau du JSON
                JSONArray jsonOnArray         =   jsonOnObject.optJSONArray("statuts");

                    // Parcour du JSON pour y récuperer les donnes et crée les status
                    for (int i = 0 ; i < jsonOnArray.length() ; i++)
                    {
                        // Création d'un objet Article a partir de l'indice de i
                        statusArrayList.add(new Status(jsonOnArray.getJSONObject(i).getInt("sId"),jsonOnArray.getJSONObject(i).getString("sContenu"),jsonOnArray.getJSONObject(i).getInt("sLike"),jsonOnArray.getJSONObject(i).getString("sHeure"),jsonOnArray.getJSONObject(i).getInt("sIsSignaled"),jsonOnArray.getJSONObject(i).getString("sDate"),jsonOnArray.getJSONObject(i).getString("sUrlProfilImage"),jsonOnArray.getJSONObject(i).getString("sUrlContenuImage"), jsonOnArray.getJSONObject(i).getInt("uId"), jsonOnArray.getJSONObject(i).getInt("cId"),jsonOnArray.getJSONObject(i).getString("auteur"), jsonOnArray.getJSONObject(i).getString("categorie"),jsonOnArray.getJSONObject(i).getInt("nbComment")));
                    }
            }

            catch (Exception e) { Log.e(TAG, "Impossible de parcourir le Json"); }


            // Renvoi la liste des article récuperer
            return statusArrayList;
        }

        return null;
    }




    /** Methode qui va renvoyer une liste de commentaires **/
    public static ArrayList<Commentaire> convertJsonOnCommentaire(String stringCommentaire)
    {
        if(stringCommentaire != null)
        {
            // Création de la liste
            ArrayList<Commentaire>  listeCommentaire    =   new ArrayList<>();

                try
                {
                    JSONObject  jsonObject  =   new JSONObject(stringCommentaire);
                    JSONArray   jsonArray   =   jsonObject.optJSONArray("commentaire");

                        for(int i = 0 ; i < jsonArray.length(); i++)
                        {
                            // Création des Objet Commentaire
                            listeCommentaire.add(new Commentaire(jsonArray.getJSONObject(i).getInt("cId"), jsonArray.getJSONObject(i).getString("cContenu"), jsonArray.getJSONObject(i).getString("cHeure"), jsonArray.getJSONObject(i).getString("cDate"), jsonArray.getJSONObject(i).getInt("sId"), jsonArray.getJSONObject(i).getInt("uId"), jsonArray.getJSONObject(i).getString("uImageUrl"), jsonArray.getJSONObject(i).getString("uPseudo")));
                        }
                }
                catch (JSONException e) { e.printStackTrace(); }

                return listeCommentaire;
        }
        return null;
    }






    /** Methode qui va exécuter la requete pour recuperer le JSON des statuts **/
    public static void getStatutsRequest(DataStatusListener dataStatusListener)
    {
        EXECUTOR.execute(new GetFillStatutsOnline("http://192.168.43.70/WYO/api/get/getFillStatuts/statuts", dataStatusListener));
    }


    /** Methode qui va envoyer la requete pour l'inscription des users **/
    public static void sendInscriptionRequest(DataSendServer dataSendServer, JSONObject jsonObject) throws UnsupportedEncodingException
    {
        EXECUTOR.execute(new ThreadPoolExecutorSendDataServer("http://192.168.43.70/WYO/api/set/setUser/", dataSendServer, jsonObject));
    }


    /** Methode qui va envoyer la requete pour l'inscription des users **/
    public static void sendConnexionRequest(DataSendServer dataSendServer, JSONObject jsonObject) throws UnsupportedEncodingException
    {
        EXECUTOR.execute(new ThreadPoolExecutorSendDataServer("http://192.168.43.70/WYO/api/get/getConnexion/", dataSendServer, jsonObject));
    }


    /** Methode qui va envoyer la requete pour l'inscription des statuts **/
    public static void sendStatutsRequest(DataSendServer dataSendServer, JSONObject jsonObject) throws UnsupportedEncodingException
    {
        EXECUTOR.execute(new ThreadPoolExecutorSendDataServer("http://192.168.43.70/WYO/api/set/setStatuts/", dataSendServer, jsonObject));
    }


    /** Methode qui va récuperer les commentaire d'un statuts clicker **/
    public static void getCommentForStatuts(DataCommentStatuts dataCommentStatuts, JSONObject jsonObject) throws UnsupportedEncodingException
    {
        EXECUTOR.execute(new ThreadPoolGetComment("http://192.168.43.70/wyo/api/get/getComment/", dataCommentStatuts, jsonObject));
    }


    /** Methode qui va supprimer un statuts **/
    public static void deleteStatuts(DataSendServer dataSendServer, JSONObject jsonObject) throws UnsupportedEncodingException
    {
        EXECUTOR.execute(new ThreadPoolExecutorSendDataServer("http://192.168.43.70/wyo/api/delete/deleteStatuts/", dataSendServer, jsonObject));
    }


// Fin de la Class
}
