package com.example.gabrielgaggini.wyo;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;


public class ProfilFragment extends Fragment implements TabLayout.OnTabSelectedListener
{
    /** Attributs de la Class **/
    // TAG QUI CORRESPOND A LA CLE POUR AVOIR ACCES A MES ARGUMENTS
    private static final    String          USER_CONNECT    = "user_connect";
    private                 String          mParam1;
    private                 TabLayout       tabLayout;
    private                 ViewPager       viewPager;
    private                 CircleImageView image_profil;
    private                 User            userConnect;
    private                 OnFragmentInteractionListener mListener;


    /** Constructeur **/
    public ProfilFragment() { }



    /** Methode static pour crée un Fragment **/
    public static ProfilFragment newInstance(User userConnect)
    {
        ProfilFragment      fragment                =   new ProfilFragment();

        // Création d'un objet Bundle qui va me permettre d'avoir accès a des data dans le Fragment
        Bundle              dataUserConnect         =   new Bundle();
        // Ajout d'arguments qui seront disponible dans tout le fragment
        dataUserConnect.putSerializable(USER_CONNECT, userConnect);

        // Ajout des argument au Fragment
        fragment.setArguments(dataUserConnect);

        return fragment;
    }




    /** Methode de la création de vue Fragment **/
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) { mParam1 = getArguments().getString(USER_CONNECT); }
    }





    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View profilFragment =   inflater.inflate(R.layout.profil_fragment, container, false);

        // Récupere les élement du Fragment
        this.tabLayout          =   (TabLayout)         profilFragment.findViewById(R.id.tab_layout);
        this.viewPager          =   (ViewPager)         profilFragment.findViewById(R.id.view_pager);
        this.image_profil       =   (CircleImageView)   profilFragment.findViewById(R.id.image_profil);




        // Je recupere les argumenet passer lors de la création du fragment dans le MainActivity
        if(getArguments() != null)
        {
            // Je crée un User en fonction des données que j'ai recu
            this.userConnect = (User) getArguments().getSerializable(USER_CONNECT);

                if(this.userConnect != null)
                {
                    // Image temporaire pour le profil
                    Picasso.get()
                            .load(this.userConnect.getuImageUrl())
                            .into(this.image_profil);
                }
        }


        // Insertion des sections
        this.tabLayout.addTab(this.tabLayout.newTab().setText("Status"));
        this.tabLayout.addTab(this.tabLayout.newTab().setText("Commentaires"));
        this.tabLayout.addTab(this.tabLayout.newTab().setText("A Propos"));

        // Parametre d'affichage
        this.tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        // Ajout d'un mode scroll si les sections depasse de l'ecran
        this.tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        this.tabLayout.addOnTabSelectedListener(this);

        // Création d'un adapter qui va etre inserer dans le view pager pour les differents Fragment
        PagerProfil     adapter =   null;

            // Vérifie que l'activity n'est pas vide
            if(getActivity() != null)
            {
                adapter     =   new PagerProfil(getActivity().getSupportFragmentManager(), this.tabLayout.getTabCount(), this.userConnect);
            }

        this.viewPager.setAdapter(adapter);

        // Methode qui va faire bouger l'indicateur au swipe des fragments
        this.viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(this.tabLayout));

        return profilFragment ;
    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) { if (mListener != null) { mListener.onFragmentInteraction(uri); } }




    /** Methode appeler au moment de l'attachement d'un Fragment **/
    @Override public void onAttach(Context context)
    {
        super.onAttach(context);

        if (context instanceof OnFragmentInteractionListener) { mListener = (OnFragmentInteractionListener) context; }
        else { throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener"); }
    }




    @Override public void onDetach()
    {
        super.onDetach();
        mListener = null;
    }




    /** Interface du Fragment **/
    public interface OnFragmentInteractionListener { void onFragmentInteraction(Uri uri); }




    /** Methode du Table Layout **/
    @Override public void onTabSelected(TabLayout.Tab tab)   { this.viewPager.setCurrentItem(tab.getPosition()); }

    @Override public void onTabUnselected(TabLayout.Tab tab) { }

    @Override public void onTabReselected(TabLayout.Tab tab) { }






// Fin de la Class
}
